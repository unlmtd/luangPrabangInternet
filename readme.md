# Luang Prabang Internet

## Get started:
Get yourself up and running:
- Buy a *green MPhone* _Net Sim_ sim card.
- Buy some credit
- Set apn = ltcnet
- Dial *131*5# - 24h 1Gb
- Check your remaining data with '*123#'

## Plans
### Laotelecom (MPhone)
#### Net sim 
---
Price | Duration | Code
--- | --- | ---
5000 LAK | 24 Hours | '*131*5#'
10,000 LAK | 7 Days | '*131*10#
50,000 LAK | 30 Days | '*131*6#'


The following bundles are soft-capped, speed will be throttled to 384 kbps on 3G and 512 kbps on 4G:

30 days: 15 GB, 150,000 kip, activation: *131*11#
30 days: 30 GB, 250,000 kip, activation: *131*12#
add-on package: 5 GB, 50,000 kip, activation: *131*21#